$(document).ready(function(){
	hideSomeTweets();
	var tilesHeight =  $('#tiles').height();
	$(window).scroll(function(){
		if(tilesHeight < $('#tiles').height()) {
			hideSomeTweets();
		};
		tilesHeight = $('#tiles').height();
	});
});

function hideSomeTweets() {
	chrome.extension.sendRequest({command: "tweets_option"}, function(response) {
		$('#tiles li').each(function(){
			var tweet = $(this);
			var title =tweet.find('a:first img').first().attr('title');
			if(response && response.black){
				response.black.split('\n').forEach(function(name){
					if(title == $.trim(name)){
						tweet.hide();
					}
				});
			}
			if(response && response.white){
				response.white.split('\n').forEach(function(name){
					if(title == $.trim(name)){
						tweet.find('.tweet').css('background', '#CCFFFF');
					}
				});
			}
		});
	});
}