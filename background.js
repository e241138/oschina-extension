chrome.extension.onRequest.addListener(
  function(request, sender, sendResponse) {
    if (request.command == "tweets_option"){
		var black_list = localStorage["oschina_tweets_black_list"];
		var white_list = localStorage["oschina_tweets_white_list"];
		sendResponse({black: black_list, white: white_list});
	} else {
		sendResponse({});
	}
});