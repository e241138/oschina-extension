function save_options() {
	var black_username = document.getElementById("black_username").value;
	var white_username = document.getElementById("white_username").value;
	localStorage["oschina_tweets_black_list"] = black_username;
	localStorage["oschina_tweets_white_list"] = white_username;
	var status = document.getElementById("status");
	status.innerHTML = "已保存";
	setTimeout(function() {
		status.innerHTML = "";
	}, 1000);
}

function restore_options() {
	var black_username = localStorage["oschina_tweets_black_list"];
	var white_username = localStorage["oschina_tweets_white_list"];
	if (black_username) {
		document.getElementById("black_username").value = black_username;
	}
	if (white_username) {
		document.getElementById("white_username").value = white_username;
	}
}

$(document).ready(function(){
	restore_options();
	$("#getUsername").click(function(){
		save_options();
	});
});
